# Windows Installation

py -3 -m venv .venv

source .venv/Scripts/activate

pip install -U pip setuptools wheel

pip install -r requirements.txt

# Linux Installation

python3 -m venv .venv

source .venv/bin/activate

pip install -U pip setuptools wheel

pip install -r requirements.txt

# Preparation

Log into Intersight.com

Go to Service Launcher > System > Settings > API Keys > Generate API Key

Use the OpenAPI schema version 2 for now

Copy the API Key ID into the .env file

Copy the Secret Key into the /key/SecretKey.txt file

# REST API Usage

python get_alarms.py

# SDK Usage

python get_alarms_sdk.py
