"""
    https://intersight.com/apidocs/apirefs/api/v1/cond/Alarms/get/
    https://github.com/movinalot/intersight-rest-api
"""
import json
import requests
import os
import dotenv
from intersight_auth import IntersightAuth

dotenv.load_dotenv("./.env")
API_KEY_ID = os.getenv("API_KEY_ID")
SECRET_KEY_FILE = "./key/SecretKey.txt"

# Create an AUTH object
AUTH = IntersightAuth(
    secret_key_filename=SECRET_KEY_FILE,
    api_key_id=API_KEY_ID
    )

# Intersight REST API Base URL
url = 'https://www.intersight.com/api/v1/cond/Alarms'
headers = {"Content-Type": "application/json", "Accept": "application/json"}

response = requests.get(url=url, headers=headers, auth=AUTH)

response.raise_for_status()
if response.status_code == 200:
    results = response.json()['Results']

    for index,result in enumerate(results):
        print(f'ALARM {index}:')
        print('    MO ID:', result['AffectedMoId'])
        print('    MO Display Name:', result['AffectedMoDisplayName'])
        print('    Fault Code:', result['Code'])
        print('    Creation Time:', result['CreationTime'])
        print('    Description:', result['Description'])
        print('    Status:', result['Acknowledge'])
else:
    print(f'Status Code: {response.status_code} - Message: {response.text}')