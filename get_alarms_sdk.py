"""
    https://pypi.org/project/intersight/#reading-an-object
    https://www.youtube.com/watch?v=5ANR1KzC2Qs
"""
import json
import intersight
import intersight.api.cond_api
from intersight_auth_sdk import get_api_client
import os
import dotenv
from intersight_auth import IntersightAuth

dotenv.load_dotenv("./.env")
API_KEY_ID = os.getenv("API_KEY_ID")
SECRET_KEY_FILE = "./key/SecretKey.txt"

api_client = get_api_client(API_KEY_ID, SECRET_KEY_FILE, endpoint="https://intersight.com")

api_instance = intersight.api.cond_api.CondApi(api_client)

api_response = api_instance.get_cond_alarm_list()

results = api_response.results

print(type(results))
print(len(results))

for index, result in enumerate(results):
    print(f'ALARM {index}:')
    print('    MO ID:', result['affected_mo_id'])
    print('    MO Display Name:', result['affected_mo_display_name'])
    print('    Fault Code:', result['code'])
    print('    Creation Time:', result['creation_time'])
    print('    Description:', result['description'])
    print('    Status:', result['acknowledge'])
